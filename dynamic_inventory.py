#!/usr/bin/env python
#
#
#  Tower format for inventory in json.
#{
#    "group": {
#        "hosts": [
#            "192.168.28.71",
#            "192.168.28.72"
#        ],
#        "vars": {
#            "ansible_ssh_user": "johndoe",
#            "ansible_ssh_private_key_file": "~/.ssh/mykey",
#            "example_variable": "value"
#        }
#    },
#    "_meta": {
#        "hostvars": {
#            "192.168.28.71": {
#                "host_specific_var": "bar"
#            },
#            "192.168.28.72": {
#                "host_specific_var": "foo"
#            }
#        }
#    }
#}
#     Alternate Tower Format for individual host
#        {
#            "name": "172.16.0.2\n172.16.0.3",
#            "description": "",
#            "enabled": true,
#            "instance_id": "",
#            "variables": "---\nOS: Red Hat(Linux)\nUnixServiceWindowSchedule: Week3-Sat-12:00-17:59"
#
#        }
#
#
#

'''
OneSource custom dynamic inventory script for Ansible, in Python.
'''

import os
import sys
import urllib2
import argparse
import socket
import io

try:
    import json
except ImportError:
    import simplejson as json
import socket
try:
    to_unicode = unicode
except NameError:
    to_unicode = str
from ansible.module_utils.basic import *
import logging
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(levelname)s - %(message)s',
                    filename='/tmp/logs_file',
                    filemode='w')
# Until here logs only to file: 'logs_file'
# Until here logs only to file: 'logs_file'

# define a new Handler to log to console as well
console = logging.StreamHandler()
# optional, set the logging level
console.setLevel(logging.INFO)
# set a format which is the same for console use
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
# tell the handler to use this format
console.setFormatter(formatter)
# add the handler to the root logger
logging.getLogger('').addHandler(console)


onesourceJson = {}
hostvars = {}
hostvars['hostvars'] = {}
metavars = []

class OneSourceInventory(object):

    def __init__(self):
        self.inventory = {}
        self.read_cli_args()

        # Called with `--list`.
        if self.args.list:
            self.inventory = self.OneSource_inventory()
        # Called with `--host [hostname]`.
        elif self.args.host:
            # Not implemented, since we return _meta info `--list`.
            self.inventory = self.empty_inventory()
        # If no groups or vars are present, return an empty inventory.
        else:
            self.inventory = self.empty_inventory()

        print json.dumps(self.inventory);



    # Empty inventory for testing.
    def empty_inventory(self):
        return {'_meta': {'hostvars': {}}}


    # OneSource inventory for testing.
    def OneSource_inventory(self):
        #read data in from file
        #onesource_raw = open('/inventory/GetUnixServiceWindowCycles.json', 'r').read()
        onesource_web = urllib2.urlopen('https://gitlab.com/Wilken42/test/raw/master/data.json')
        onesource_raw = onesource_web.read()
#        with open('/inventory/GetUnixServiceWindowCycles.json') as f:
#            data = json.load(f)
        #Load data into the Ansible Tower Format
        for i in range(len(json.loads(onesource_raw))):
            logging.info('Group creation Host')
            logging.info(i)
            '''Group aggregator Block
            This iterates through the OneSource dicts and adds groups and hosts in that groupself.
            First if checks if the field is blank and moves on
            Second if checks if the group has already been created if it has it adds the host.
            Finally if neither of the two above is true it creates the group and adds the host
            '''
            try:
                if json.loads(onesource_raw)[i]['Location'] == "":
                    pass
                elif json.loads(onesource_raw)[i]['Location'] in onesourceJson:
                    onesourceJson[json.loads(onesource_raw)[i]['Location']].append(json.loads(onesource_raw)[i]['ServerName'])
                else:
                    onesourceJson[json.loads(onesource_raw)[i]['Location']] = []
                    onesourceJson[json.loads(onesource_raw)[i]['Location']].append(json.loads(onesource_raw)[i]['ServerName'])
            except:
                pass
            try:
                if json.loads(onesource_raw)[i]['Appcode'] == "":
                    pass
                elif json.loads(onesource_raw)[i]['Appcode'] in onesourceJson:
                    onesourceJson[json.loads(onesource_raw)[i]['Appcode']].append(json.loads(onesource_raw)[i]['ServerName'])
                else:
                    onesourceJson[json.loads(onesource_raw)[i]['Appcode']] = []
                    onesourceJson[json.loads(onesource_raw)[i]['Appcode']].append(json.loads(onesource_raw)[i]['ServerName'])
            except:
                pass
            try:
                if json.loads(onesource_raw)[i]['OS'] == "":
                    pass
                elif json.loads(onesource_raw)[i]['OS'] in onesourceJson:
                    onesourceJson[json.loads(onesource_raw)[i]['OS']].append(json.loads(onesource_raw)[i]['ServerName'])
                else:
                    onesourceJson[json.loads(onesource_raw)[i]['OS']] = []
                    onesourceJson[json.loads(onesource_raw)[i]['OS']].append(json.loads(onesource_raw)[i]['ServerName'])
            except:
                pass
            try:
                if json.loads(onesource_raw)[i]['OSType'] == "":
                    pass
                elif json.loads(onesource_raw)[i]['OSType'] in onesourceJson:
                    onesourceJson[json.loads(onesource_raw)[i]['OSType']].append(json.loads(onesource_raw)[i]['ServerName'])
                else:
                    onesourceJson[json.loads(onesource_raw)[i]['OSType']] = []
                    onesourceJson[json.loads(onesource_raw)[i]['OSType']].append(json.loads(onesource_raw)[i]['ServerName'])
            except:
                pass
            try:
                if json.loads(onesource_raw)[i]['UnixServiceWindowSchedule'] == "":
                    pass
                elif json.loads(onesource_raw)[1]['UnixServiceWindowSchedule'] in onesourceJson:
                    onesourceJson[json.loads(onesource_raw)[i]['UnixServiceWindowSchedule']].append(json.loads(onesource_raw)[i]['ServerName'])
                else:
                    onesourceJson[json.loads(onesource_raw)[i]['UnixServiceWindowSchedule']] = []
                    onesourceJson[json.loads(onesource_raw)[i]['UnixServiceWindowSchedule']].append(json.loads(onesource_raw)[i]['ServerName'])
            except:
                pass
            try:
                if json.loads(onesource_raw)[i]['ServerEnvironment'] == "":
                    pass
                elif json.loads(onesource_raw)[1]['ServerEnvironment'] in onesourceJson:
                    onesourceJson[json.loads(onesource_raw)[i]['ServerEnvironment']].append(json.loads(onesource_raw)[i]['ServerName'])
                else:
                    onesourceJson[json.loads(onesource_raw)[i]['ServerEnvironment']] = []
                    onesourceJson[json.loads(onesource_raw)[i]['ServerEnvironment']].append(json.loads(onesource_raw)[i]['ServerName'])
            except:
                pass
            '''Hostvars block
            This generates the hostvars for a server, that are then added into the meta section
            '''
            tmpHostVars={}
            try:
                if json.loads(onesource_raw)[i]['ServerEnvironment'] == "":
                    pass
                else:
                    tmpHostVars["ServerEnvironment"] = json.loads(onesource_raw)[i]['ServerEnvironment']
            except:
                pass
            try:
                if json.loads(onesource_raw)[i]['Appcode'] == "":
                    pass
                else:
                    tmpHostVars["Appcode"] = json.loads(onesource_raw)[i]['Appcode']
            except:
                pass
            try:
                if json.loads(onesource_raw)[i]['ApplicationCoordinator_ENUM'] == "":
                    pass
                else:
                    tmpHostVars["ApplicationCoordinator_ENUM"] = json.loads(onesource_raw)[i]['ApplicationCoordinator_ENUM']
            except:
                pass
            try:
                if json.loads(onesource_raw)[i]['Application_Coordinator'] == "":
                    pass
                else:
                    tmpHostVars["Application_Coordinator"] = json.loads(onesource_raw)[i]['Application_Coordinator']
            except:
                pass
            try:
                if json.loads(onesource_raw)[i]['ITCustodian_ENUM'] == "":
                    pass
                else:
                    tmpHostVars["ITCustodian_ENUM"] = json.loads(onesource_raw)[i]['ITCustodian_ENUM']
            except:
                pass
            try:
                if json.loads(onesource_raw)[i]['Location'] == "":
                    ## changed as some patching requires this fact
                    tmpHostVars["Location"] = ""
                else:
                    tmpHostVars["Location"] = json.loads(onesource_raw)[i]['Location']
            except:
                pass
            try:
                if json.loads(onesource_raw)[i]['OS'] == "":
                    pass
                else:
                    tmpHostVars["OS"] = json.loads(onesource_raw)[i]['OS']
            except:
                pass
            try:
                if json.loads(onesource_raw)[i]['UnixServiceWindowSchedule'] == "":
                    pass
                else:
                    tmpHostVars["UnixServiceWindowSchedule"] = json.loads(onesource_raw)[i]['UnixServiceWindowSchedule']
            except:
                pass
            try:
                if json.loads(onesource_raw)[i]['OSType'] == "":
                    pass
                else:
                    tmpHostVars["OSType"] = json.loads(onesource_raw)[i]['OSType']
            except:
                pass
            #Adding servers to the meta/hostvars section, and adding hostvars to those servers.
            try:
                if json.loads(onesource_raw)[i]['ServerName'] == "":

                    pass
                else:
                    logging.info('Var creation Host')
                    logging.info(i)
                    tmpHost = {json.loads(onesource_raw)[i]['ServerName']: tmpHostVars}
                    # Placeholder for code to use fqdn lookup if needed
                    ###tmpHost = {socket.getfqdn(json.loads(onesource_raw)[i]['ServerName']): tmpHostVars}
                    hostvars['hostvars'].update(tmpHost)
                    onesourceJson['_meta'] = hostvars
            except:
                pass
        return onesourceJson

    # Read the command line args passed to the script.
    def read_cli_args(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('--list', action = 'store_true')
        parser.add_argument('--host', action = 'store')
        self.args = parser.parse_args()

# Get the inventory.
OneSourceInventory()
